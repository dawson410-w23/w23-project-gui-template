# Study Buddy Project GUI example


## GUI Example

The purpose of this repository is to serve as an example of how one can present data from your application using Avalonia. The project depicts a Study Buddy finder application. It features , where people can create Profiles that describe the courses they are studying, as well as create Events related to the courses they're enrolled in. It has a directory of dummy models that represent simple data for these entities, which are then presented through corresponding Views and ViewModels. 

The **main** branch features views and associated viewmodels for logging users in, showing and editing profiles, and showing and editing events. 

The **navigation** branch demonstrates different means for handling navigation between different views (notably, using a navigation bar).

Recommendation: Use this code as a reference when implementing your own Views and ViewModels within your existing app. Note, in our project there are a lot of parallels between Recipes and the Events presented here, as well as between our Users and the Users/Profiles here.