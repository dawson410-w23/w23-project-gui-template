﻿using System;
using System.Reactive.Linq;
using ReactiveUI;
using StudyApp.Models;

namespace StudyApp.ViewModels
{
    class MainWindowViewModel : ViewModelBase
    {
        ViewModelBase content;
        User? LoggedInUser;

        public ViewModelBase Content
        {
            get => content;
            private set => this.RaiseAndSetIfChanged(ref content, value);
        }


        public MainWindowViewModel()
        {

            LogInViewModel vm = new LogInViewModel();
            
            vm.Login.Subscribe(x => {PrepareMainPage(vm.LoginUser());});
            vm.Register.Subscribe(x => {PrepareMainPage(vm.RegisterUser());});
            Content = vm;
        }


        public void PrepareMainPage(User u){
            LoggedInUser = u;
            Profile p = new Profile(u);
            Content = new ProfileDisplayViewModel(p);
        }
       

        public void ViewEvent()
        {
            Content = new EventDisplayViewModel(new Event()) ;
        }

        public void EditEvent()
        {
            EventDisplayViewModel dispvm = (EventDisplayViewModel) Content;
            var vm = new EventEditViewModel(dispvm.Event);
            
            vm.Ok.Subscribe(x => {Content = dispvm;});
            Content = vm;
        }

        public void EditProfile()
        {
            ProfileDisplayViewModel dispvm = (ProfileDisplayViewModel) Content;
            var vm = new ProfileEditViewModel(dispvm.Profile);
            
            vm.Ok.Subscribe(x => {Content = dispvm;});
            Content = vm;
        }
    }
}