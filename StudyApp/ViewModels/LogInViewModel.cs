using StudyApp.Models;
using ReactiveUI;
using System.Reactive;

namespace StudyApp.ViewModels
{
    public class LogInViewModel : ViewModelBase
    {
        public string Username {get; set;}
        public string Password {get; set;}

        public ReactiveCommand<Unit, Unit> Login { get; } = ReactiveCommand.Create(() => { });

        public ReactiveCommand<Unit, Unit> Register { get; } = ReactiveCommand.Create(() => { });

        public User? User { get; private set;}
        public User RegisterUser(){
            this.User = new User(Username, Password);
            return this.User;
        }

        public User LoginUser(){
            this.User = new User(Username, Password);
            return this.User;
        }



    }
}